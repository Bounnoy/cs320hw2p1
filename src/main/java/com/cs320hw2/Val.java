package com.cs320hw2;

public class Val extends Expr {

    protected int value;

    public Val(int value) {
        this.value = value;
    }

    public int evaluate() {
        return value;
    }

    public void display() {
        System.out.print(value);
    }
}
