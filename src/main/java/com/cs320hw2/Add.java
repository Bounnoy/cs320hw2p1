package com.cs320hw2;

public class Add extends Expr {

    protected Expr left;
    protected Expr right;

    public Add(Expr left, Expr right) {
        this.left = left;
        this.right = right;
    }
    public int evaluate() {
        if (left.evaluate() == -1 || right.evaluate() == -1)
            return -1;

        return left.evaluate() + right.evaluate();
    }

    public void display() {
        System.out.print("(");
        left.display();
        System.out.print(" + ");
        right.display();
        System.out.print(")");
    }
}
