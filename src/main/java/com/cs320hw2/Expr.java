package com.cs320hw2;

abstract class Expr {

    abstract public int evaluate();
    abstract public void display();

}
