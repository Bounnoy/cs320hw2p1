package com.cs320hw2

class ExprTest extends GroovyTestCase {
    void testEvaluate() {

        // (1 + 2) * 3
        Expr x = new Multiply(new Add(new Val(1), new Val(2)), new Val(3))
        assertEquals 9, x.evaluate()

        // 4 / (3 - 1) DIVISION OK
        Expr y = new Divide(new Val(4), new Subtract(new Val(3), new Val(1)))
        assertEquals 2, y.evaluate()

        // 4 / (1 * 3) DIVISION NOT OK
        Expr z = new Divide(new Val(4), new Multiply(new Val(1), new Val(3)))
        assertEquals (-1, z.evaluate())

    }

    void testDisplay() {

        // Make console prints write to output stream so we can test display value.
        ByteArrayOutputStream outContent = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outContent))

        // (1 + 2) * 3
        Expr x = new Multiply(new Add(new Val(1), new Val(2)), new Val(3))
        x.display()
        assertEquals "((1 + 2) * 3)", outContent.toString()

        outContent.reset()

        // 4 / (3 - 1) DIVISION OK
        Expr y = new Divide(new Val(4), new Subtract(new Val(3), new Val(1)))
        y.display()
        assertEquals "(4 / (3 - 1))", outContent.toString()

        outContent.reset()

        // 4 / (1 * 3) DIVISION NOT OK
        Expr z = new Divide(new Val(4), new Multiply(new Val(1), new Val(3)))
        z.display()
        assertEquals "error", outContent.toString()

    }
}
