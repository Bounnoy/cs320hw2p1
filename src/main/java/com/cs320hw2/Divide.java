package com.cs320hw2;

public class Divide extends Expr {
    protected Expr left;
    protected Expr right;

    public Divide(Expr left, Expr right) {
        this.left = left;
        this.right = right;
    }
    public int evaluate() {

        if (left.evaluate() == 0 || right.evaluate() == 0)
            return -1;

        return left.evaluate() % right.evaluate() == 0 ? left.evaluate() / right.evaluate() : -1;
    }

    public void display() {
        if (evaluate() > -1) {
            System.out.print("(");
            left.display();
            System.out.print(" / ");
            right.display();
            System.out.print(")");
        } else
            System.out.print("error");
    }
}
